#ifndef GAMEMODEL_H
#define GAMEMODEL_H


// cclibrary
#include <engine.h>

// qt
#include <QAbstractListModel>
#include <QPoint>


// stl
#include <memory>



class GameModel : public QAbstractListModel {
  Q_OBJECT
public:
  Q_PROPERTY(int boardSize READ boardSize NOTIFY boardSizeChanged)
  Q_PROPERTY(uint currentPlayer READ currentPlayer NOTIFY currentPlayerChanged)
  Q_PROPERTY(uint currentPlayerType READ currentPlayerType NOTIFY currentPlayerType)
//  Q_PROPERTY(bool legalMovesCheck READ legalMovesCheck NOTIFY legalMovesCheck)

  enum ModelRoles { OccupiedRole = Qt::UserRole + 1, PlayerNrRole, PieceNrRole, legalMoveRole };



  GameModel(const othello::OthelloGameEngine& game_engine,
            QObject*                          parent = nullptr);
  ~GameModel() override = default;

  int      rowCount(const QModelIndex& parent = QModelIndex()) const override;
  QVariant data(const QModelIndex& index, int role) const override;
  QHash<int, QByteArray> roleNames() const override;


public slots:
  void update();

private:
  const othello::OthelloGameEngine& m_game_engine;

  int boardSize() const;
  uint currentPlayer() const;
  uint currentPlayerType() const;

signals:
  void boardSizeChanged(size_t);
  void currentPlayerChanged(size_t);
  void currentPlayerType(size_t);
};





#endif   // GAMEMODEL_H
