#ifndef GUIAPPLICATION_H
#define GUIAPPLICATION_H


// local
#include "gamemodel.h"

// othello library
#include <engine.h>

// qt
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QSizeF>
#include <QFutureWatcher>
#include <QtConcurrent>

// stl
#include <memory>


class GuiApplication : public QGuiApplication {
  Q_OBJECT
public:
  GuiApplication(int& argc, char** argv);
  ~GuiApplication() override = default;

private:
  std::array<othello::OthelloGameEngine,1> m_game_engine;
  GameModel m_model;
  QQmlApplicationEngine m_app;
  QFutureWatcher<void> m_watcher;
  bool end = false;
  bool started = false;

private slots:
  void initNewHumanGame();
  void initNewGameVsAI(int AI_player, int AI_type);
  void initNewSimulationGame(int AI_one_type, int AI_two_type);
  void endGameAndQuit();
  void endOfGameActions();
  void startNextTurn();
  void newAITurn();
  void boardChanged(int board_pos);
  void debugApp();

signals:
  void enqueueNextTurn();
  void gameEnded();
  void displayFinalScores(int player_one_score, int player_two_score);

};   // END class GuiApplication

#endif   // GUIAPPLICATION_H
