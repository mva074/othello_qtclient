#include "guiapplication.h"

// othello game library
#include <orangemonkey_ai.h>
#include <minimax_ai.h>
#include <alphabeta_ai.h>


// qt
#include <QQmlContext>
#include <QQuickItem>
#include <QQuickWindow>
#include <QTimer>
#include <QDebug>
#include <QThread>


// stl
#include <chrono>
using namespace std::chrono_literals;


GuiApplication::GuiApplication(int& argc, char** argv)
  : QGuiApplication(argc, argv),
    m_game_engine{}, m_model{m_game_engine[0]}, m_app{}
{

  m_app.rootContext()->setContextProperty("gamemodel", &m_model);

  m_app.load(QUrl("qrc:/qml/gui.qml"));

  m_game_engine[0].initialInterface();
  m_model.update();

  auto* root_window = qobject_cast<QQuickWindow*>(m_app.rootObjects().first());
  if (root_window) {

    connect(root_window, SIGNAL(endGameAndQuit()), this,
            SLOT(endGameAndQuit()));

    connect(root_window, SIGNAL(initNewHumanGame()), this,
            SLOT(initNewHumanGame()));

    connect(root_window, SIGNAL(initNewGameVsAI(int, int)), this,
            SLOT(initNewGameVsAI(int, int)));

    connect(root_window, SIGNAL(initNewSimulationGame(int, int)), this,
            SLOT(initNewSimulationGame(int, int)));

    connect(root_window, SIGNAL(boardClicked(int)), this,
            SLOT(boardChanged(int)));

    connect(this, SIGNAL(displayFinalScores(int, int)), root_window,
            SIGNAL(displayFinalScores(int, int)));

    connect(this, &GuiApplication::gameEnded, this,
            &GuiApplication::endOfGameActions);

    connect(this, &GuiApplication::enqueueNextTurn, this,
             &GuiApplication::startNextTurn );

    connect(&m_watcher, &QFutureWatcher<void>::finished,
             this, &GuiApplication::newAITurn);
  }
}
void GuiApplication::startNextTurn()

{
    m_game_engine[0].switchPlayer();
    int moves = m_game_engine[0].checkMoves();
    m_model.update();

//    char c[20];
//    std::string str;
//    str = "Valid moves: " + std::to_string(moves);
//    strcpy_s(c, str.c_str());
//    qDebug() << c;

    if (moves > 0){
        m_game_engine[0].passedThisTurn(false);
        //m_game_engine[0].think(2s);
    }
    else
    {
        qDebug() << "You don't have valid moves";

        if (m_game_engine[0].passedLastTurn() == true)
        {
            // end of the game
            emit gameEnded();
            return;
        }
            else
        {
        m_game_engine[0].passedThisTurn(true);
        }
        emit enqueueNextTurn();
    }

    if (m_game_engine[0].currentPlayerType() == othello::PlayerType::AI && end == false){
        const auto ai_max_time = 15s;
        QFuture<void> future
                      = QtConcurrent::map(m_game_engine, [ai_max_time](auto& engine)
        { return engine.think(ai_max_time); });
        m_watcher.setFuture(future);
//      newAITurn();
    }

}

void GuiApplication::newAITurn()
{
        m_game_engine[0].performAIMove();
        m_model.update();
        emit enqueueNextTurn();
}

void GuiApplication::boardChanged(int board_pos){

    if (m_game_engine[0].currentPlayerType() == othello::PlayerType::AI || !started) {
        return;
    }
    othello::BitPos pos{board_pos};
    if (m_game_engine[0].performMoveForCurrentHuman(pos)) {
        m_model.update();
        startNextTurn();
        return;
    }
    else {
        qDebug() << "Not valid move!";

        //debugApp();
        return;
    }
}

void GuiApplication::debugApp()
{
    int moves = m_game_engine[0].checkMoves();
    char c[20];
    std::string str;
    str = "Valid moves: " + std::to_string(moves);
    strcpy_s(c, str.c_str());
    qDebug() << c;
}


void GuiApplication::initNewHumanGame()
{
    started = true;
    end = false;
    m_game_engine[0].initPlayerType<othello::HumanPlayer, othello::PlayerId::One>();
    m_game_engine[0].initPlayerType<othello::HumanPlayer, othello::PlayerId::Two>();

    m_game_engine[0].initNewGame();
    m_model.update();

    emit enqueueNextTurn();
}

void GuiApplication::initNewGameVsAI(int AI_player, int AI_type)
{
    started = true;
    end = false;
//    std::string ColorAI = AI_color.toStdString();
    if (AI_player == 1){
        m_game_engine[0].initPlayerType<othello::HumanPlayer, othello::PlayerId::One>();
        switch (AI_type) {
        case 0:
            m_game_engine[0].initPlayerType<othello::monkey_ais::OrangeMonkeyAI, othello::PlayerId::Two>();
            break;
        case 1:
            m_game_engine[0].initPlayerType<othello::minimax_ais::MinimaxAI, othello::PlayerId::Two>();
            break;
        case 2:
            m_game_engine[0].initPlayerType<othello::alphabeta_ais::AlphabetaAI, othello::PlayerId::Two>();
            break;
        }
    }
    else {
        switch (AI_type) {
        case 0:
            m_game_engine[0].initPlayerType<othello::monkey_ais::OrangeMonkeyAI, othello::PlayerId::One>();
            break;
        case 1:
            m_game_engine[0].initPlayerType<othello::minimax_ais::MinimaxAI, othello::PlayerId::One>();
            break;
        case 2:
            m_game_engine[0].initPlayerType<othello::alphabeta_ais::AlphabetaAI, othello::PlayerId::One>();
            break;
        }

        m_game_engine[0].initPlayerType<othello::HumanPlayer, othello::PlayerId::Two>();
    }

    m_game_engine[0].initNewGame();
    m_model.update();

    emit enqueueNextTurn();
}

void GuiApplication::initNewSimulationGame(int AI_one_type, int AI_two_type)
{
    started = true;
    end = false;

    switch (AI_one_type) {
    case 0:
        m_game_engine[0].initPlayerType<othello::monkey_ais::OrangeMonkeyAI, othello::PlayerId::One>();
        break;
    case 1:
        m_game_engine[0].initPlayerType<othello::minimax_ais::MinimaxAI, othello::PlayerId::One>();
        break;
    case 2:
        m_game_engine[0].initPlayerType<othello::alphabeta_ais::AlphabetaAI, othello::PlayerId::One>();
        break;
    }

    switch (AI_two_type) {
    case 0:
        m_game_engine[0].initPlayerType<othello::monkey_ais::OrangeMonkeyAI, othello::PlayerId::Two>();
        break;
    case 1:
        m_game_engine[0].initPlayerType<othello::minimax_ais::MinimaxAI, othello::PlayerId::Two>();
        break;
    case 2:
        m_game_engine[0].initPlayerType<othello::alphabeta_ais::AlphabetaAI, othello::PlayerId::Two>();
        break;
    }

    m_game_engine[0].initNewGame();
    m_model.update();

    emit enqueueNextTurn();
}

void GuiApplication::endGameAndQuit() {
    QGuiApplication::quit();
}


void GuiApplication::endOfGameActions()
{
    int player_one_score, player_two_score;
    end = true;
    player_one_score = m_game_engine[0].getScore(othello::PlayerId::One);
    player_two_score = m_game_engine[0].getScore(othello::PlayerId::Two);
    emit displayFinalScores(player_one_score, player_two_score);
    m_game_engine[0].initPlayerType<othello::HumanPlayer, othello::PlayerId::One>();
    m_game_engine[0].initPlayerType<othello::HumanPlayer, othello::PlayerId::Two>();
    m_model.update();
}
