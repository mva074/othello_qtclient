import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.4

ApplicationWindow {
    id: root

    signal initNewHumanGame()
    signal initNewGameVsAI(int ai_player, int ai_type)
    signal initNewSimulationGame(int ai_one_type, int ai_two_type)
    signal endGameAndQuit()
    signal boardClicked(int board_pos)
    signal displayFinalScores(int player_one_score, int player_two_score)
    signal customedColors()

    onDisplayFinalScores: {
        final_score_window.player_one_final_score = player_one_score
        final_score_window.player_two_final_score = player_two_score
        final_score_window.visible = true
    }

    onCustomedColors: {
        custom_color_window.visible = true
    }

    onInitNewGameVsAI: {
        custom_color_window.visible = false
        final_score_window.visible = false
    }

    onInitNewHumanGame: {
        custom_color_window.visible = false
        final_score_window.visible = false
    }

    onInitNewSimulationGame: {
        custom_color_window.visible = false
        final_score_window.visible = false
    }

    visible: true
    property string player_one_color: "orange"
    property string player_two_color: "blue"
    property string field_color: "white"
    property string legal_move_color: "grey"

    width: 800
    height: 600

    menuBar: MenuBar {
        Menu {
            title: "Game"
            MenuItem {
                text: "New Game: Human Endeavour"
                onTriggered: initNewHumanGame()
            }
            MenuItem {
                text: "New Game: Human vs Orange Monkey"
                property int ai_player: 1
                property int ai_type: 0
                onTriggered: initNewGameVsAI(ai_player, ai_type)
            }
            MenuItem {
                text: "New Game: Orange Monkey vs Human"
                property int ai_player: 0
                property int ai_type: 0
                onTriggered: initNewGameVsAI(ai_player, ai_type)
            }
            MenuItem {
                text: "New Game: Human vs Minimax"
                property int ai_player: 1
                property int ai_type: 1
                onTriggered: initNewGameVsAI(ai_player, ai_type)
            }
            MenuItem {
                text: "New Game: Minimax vs Human"
                property int ai_player: 0
                property int ai_type: 1
                onTriggered: initNewGameVsAI(ai_player, ai_type)
            }
            MenuItem {
                text: "New Game: Human vs Alphabeta"
                property int ai_player: 1
                property int ai_type: 2
                onTriggered: initNewGameVsAI(ai_player, ai_type)
            }
            MenuItem {
                text: "New Game: Alphabeta vs Human"
                property int ai_player: 0
                property int ai_type: 2
                onTriggered: initNewGameVsAI(ai_player, ai_type)
            }
            MenuItem {
                text: "Game Settings"
                onTriggered: customedColors()
            }
            MenuItem {
                text: "Quit"
                onTriggered: endGameAndQuit()
            }
        }

        Menu {
            title: "Simulation"
            MenuItem {
                text: "New Game: Orange Monkey vs Orange Monkey"
                property int ai_one_type: 0
                property int ai_two_type: 0
                onTriggered: initNewSimulationGame(ai_one_type, ai_two_type)
            }
            MenuItem {
                text: "New Game: Minimax vs Minimax"
                property int ai_one_type: 1
                property int ai_two_type: 1
                onTriggered: initNewSimulationGame(ai_one_type, ai_two_type)
            }
            MenuItem {
                text: "New Game: Alphabeta vs Alphabeta"
                property int ai_one_type: 2
                property int ai_two_type: 2
                onTriggered: initNewSimulationGame(ai_one_type, ai_two_type)
            }
            MenuItem {
                text: "New Game: Orange Monkey vs Minimax"
                property int ai_one_type: 0
                property int ai_two_type: 1
                onTriggered: initNewSimulationGame(ai_one_type, ai_two_type)
            }
            MenuItem {
                text: "New Game: Minimax vs Orange Monkey"
                property int ai_one_type: 1
                property int ai_two_type: 0
                onTriggered: initNewSimulationGame(ai_one_type, ai_two_type)
            }
            MenuItem {
                text: "New Game: Orange Monkey vs Alphabeta"
                property int ai_one_type: 0
                property int ai_two_type: 2
                onTriggered: initNewSimulationGame(ai_one_type, ai_two_type)
            }
            MenuItem {
                text: "New Game: Alphabeta vs Orange Monkey"
                property int ai_one_type: 2
                property int ai_two_type: 0
                onTriggered: initNewSimulationGame(ai_one_type, ai_two_type)
            }
            MenuItem {
                text: "New Game: Minimax vs Alphabeta"
                property int ai_one_type: 1
                property int ai_two_type: 2
                onTriggered: initNewSimulationGame(ai_one_type, ai_two_type)
            }
            MenuItem {
                text: "New Game: Alphabeta vs Minimax"
                property int ai_one_type: 2
                property int ai_two_type: 1
                onTriggered: initNewSimulationGame(ai_one_type, ai_two_type)
            }
        }
    }


    GridLayout {
        anchors.fill: parent
        columns: 8
        rows: 8
        columnSpacing: 0.2
        rowSpacing: 0.2


        Repeater {
            model: VisualDataModel {
                model: gamemodel
                delegate: Rectangle {

                    id: piece_rec
                    property bool contains_mouse: false

                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    color: {

                        if (occupied) {
                            if(playernr == 0) return player_one_color
                            else if(playernr == 1) return player_two_color
                            else return "black"
                        }
                        else if(contains_mouse) {
                            if(gamemodel.currentPlayer === 0) return player_one_color
                            else if(gamemodel.currentPlayer  === 1) return player_two_color
                            else return "black"
                        }
                        else if (legalMove){
                            return legal_move_color
                        }
                        else return field_color
                    }

                    border.color: field_color === "black" ? "white" : "black"
                    border.width: 1

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.debug("Piece nr: " + piecenr)
                            boardClicked(piecenr)
                        }

                        hoverEnabled: true
                        onContainsMouseChanged: piece_rec.contains_mouse = containsMouse

                    }
                }
            }
        }
    }

    Rectangle {
        id: final_score_window
        visible: false

        property int player_one_final_score: 0
        property int player_two_final_score: 0

        anchors.fill: parent
        anchors.margins: 50

        color: "red"
        opacity: 0.8

        ColumnLayout {
            anchors.fill: parent
            anchors.margins: 20

            Text{ Layout.preferredWidth: parent.width; height: 20;
                text: "Game Over!"; font.family: "Garamond"; font.pointSize: 32;
                horizontalAlignment: "AlignHCenter"}
            Item{ height: 20}
            Text{ Layout.preferredWidth: parent.width; height: 20;
                text: "Player one: " + final_score_window.player_one_final_score;
                font.family: "Garamond"; font.pointSize: 24;
                horizontalAlignment: "AlignHCenter"}
            Text{ Layout.preferredWidth: parent.width; height: 20;
                text: "Player two: " + final_score_window.player_two_final_score;
                font.family: "Garamond"; font.pointSize: 24;
                horizontalAlignment: "AlignHCenter"}
            Item{ Layout.fillHeight: true}
            Button{ text: "Ok"; onClicked: final_score_window.visible = false }
        }

    }

    Rectangle {
        id : custom_color_window
        visible : true

        anchors.fill: parent
        anchors.margins: 50
        color: "grey"
        opacity: 0.8


        ColumnLayout {
            anchors.fill: parent
            anchors.margins: 20
            Text{ Layout.preferredWidth: parent.width; height: 20;
                text: "Game Settings"; color: "red";
                font.family: "Garamond"; font.pointSize: 32;
                horizontalAlignment:  "AlignHCenter"}

            Item{ height: 20}
            Text{ Layout.preferredWidth: parent.width; height: 20;
                text: "Player One Color"; font.family: "Garamond";
                font.pointSize: 18; color: "red";}
            Row {
                Button {
                    text: "Orange";
                    enabled: player_two_color != "orange" &
                             player_one_color != "orange" &
                             field_color != "orange" ? true : false
                    onClicked: player_one_color = "orange"
                }
                Button {
                    text: "Blue";
                    enabled: player_two_color != "blue" &
                             player_one_color != "blue" &
                             field_color != "blue" ? true : false
                    onClicked: player_one_color = "blue"
                }
                Button {
                    text: "Red";
                    enabled: player_two_color != "red" &
                             player_one_color != "red" &
                             field_color != "red" ? true : false
                    onClicked: player_one_color = "red"
                }
                Button {
                    text: "Green";
                    enabled: player_two_color != "green" &
                             player_one_color != "green" &
                             field_color != "green" ? true : false
                    onClicked: player_one_color = "green"
                }
                Button {
                    text: "White";
                    enabled: player_two_color != "white" &
                             player_one_color != "white" &
                             field_color != "white" ? true : false
                    onClicked: player_one_color = "white"
                }
                Button {
                    text: "Yellow";
                    enabled: player_two_color != "yellow" &
                             player_one_color != "yellow" &
                             field_color != "yellow" ? true : false
                    onClicked: player_one_color = "yellow"
                }
                Button {
                    text: "Black";
                    enabled: player_two_color != "black" &
                             player_one_color != "black" &
                             field_color != "black" ? true : false
                    onClicked: player_one_color = "black"
                }
                Button {
                    text: "Purple";
                    enabled: player_two_color != "purple" &
                             player_one_color != "purple" &
                             field_color != "purple" ? true : false
                    onClicked: player_one_color = "purple"
                }
                Button {
                    text: "Cyan";
                    enabled: player_two_color != "cyan" &
                             player_one_color != "cyan" &
                             field_color != "cyan" ? true : false
                    onClicked: player_one_color = "cyan"
                }
            }

            Text{ Layout.preferredWidth: parent.width; height: 20;
                text: "Player Two Color"; font.family: "Garamond";
                font.pointSize: 18; color: "red";}
            Row {
                Button {
                    text: "Orange";
                    enabled: player_two_color != "orange" &
                             player_one_color != "orange" &
                             field_color != "orange" ? true : false
                    onClicked: player_two_color = "orange"
                }
                Button {
                    text: "Blue";
                    enabled: player_two_color != "blue" &
                             player_one_color != "blue" &
                             field_color != "blue" ? true : false
                    onClicked: player_two_color = "blue"
                }
                Button {
                    text: "Red";
                    enabled: player_two_color != "red" &
                             player_one_color != "red" &
                             field_color != "red" ? true : false
                    onClicked: player_two_color = "red"
                }
                Button {
                    text: "Green";
                    enabled: player_two_color != "green" &
                             player_one_color != "green" &
                             field_color != "green" ? true : false
                    onClicked: player_two_color = "green"
                }
                Button {
                    text: "White";
                    enabled: player_two_color != "white" &
                             player_one_color != "white" &
                             field_color != "white" ? true : false
                    onClicked: player_two_color = "white"
                }
                Button {
                    text: "Yellow";
                    enabled: player_two_color != "yellow" &
                             player_one_color != "yellow" &
                             field_color != "yellow" ? true : false
                    onClicked: player_two_color = "yellow"
                }
                Button {
                    text: "Black";
                    enabled: player_two_color != "black" &
                             player_one_color != "black" &
                             field_color != "black" ? true : false
                    onClicked: player_two_color = "black"
                }
                Button {
                    text: "Purple";
                    enabled: player_two_color != "purple" &
                             player_one_color != "purple" &
                             field_color != "purple" ? true : false
                    onClicked: player_two_color = "purple"
                }
                Button {
                    text: "Cyan";
                    enabled: player_two_color != "cyan" &
                             player_one_color != "cyan" &
                             field_color != "cyan" ? true : false
                    onClicked: player_two_color = "cyan"
                }
            }

            Text{ Layout.preferredWidth: parent.width; height: 20;
                text: "Field color"; font.family: "Garamond";
                font.pointSize: 18; color: "red";}
            Row {
                Button {
                    text: "Orange";
                    enabled: player_two_color != "orange" &
                             player_one_color != "orange" &
                             field_color != "orange" ? true : false
                    onClicked: {
                        legal_move_color = "light salmon"
                        field_color = "orange"
                    }
                }
                Button {
                    text: "Blue";
                    enabled: player_two_color != "blue" &
                             player_one_color != "blue" &
                             field_color != "blue" ? true : false
                    onClicked: {
                        legal_move_color = "light blue"
                        field_color = "blue"
                    }
                }
                Button {
                    text: "Red";
                    enabled: player_two_color != "red" &
                             player_one_color != "red" &
                             field_color != "red" ? true : false
                    onClicked: {
                        legal_move_color = "light coral"
                        field_color = "red"
                    }
                }
                Button {
                    text: "Green";
                    enabled: player_two_color != "green" &
                             player_one_color != "green" &
                             field_color != "green" ? true : false
                    onClicked: {
                        legal_move_color = "light green"
                        field_color = "green"
                    }
                }
                Button {
                    text: "White";
                    enabled: player_two_color != "white" &
                             player_one_color != "white" &
                             field_color != "white" ? true : false
                    onClicked: {
                        legal_move_color = "light grey"
                        field_color = "white"
                    }
                }
                Button {
                    text: "Yellow";
                    enabled: player_two_color != "yellow" &
                             player_one_color != "yellow" &
                             field_color != "yellow" ? true : false
                    onClicked: {
                        legal_move_color = "light yellow"
                        field_color = "yellow"
                    }
                }
                Button {
                    text: "Black";
                    enabled: player_two_color != "black" &
                             player_one_color != "black" &
                             field_color != "black" ? true : false
                    onClicked: {
                        legal_move_color = "dim grey"
                        field_color = "black"
                    }
                }
                Button {
                    text: "Purple";
                    enabled: player_two_color != "purple" &
                             player_one_color != "purple" &
                             field_color != "purple" ? true : false
                    onClicked: {
                        legal_move_color = "medium purple"
                        field_color = "purple"
                    }
                }
                Button {
                    text: "Cyan";
                    enabled: player_two_color != "cyan" &
                             player_one_color != "cyan" &
                             field_color != "cyan" ? true : false
                    onClicked: {
                        legal_move_color = "light sky blue"
                        field_color = "cyan"
                    }
                }
            }
            Item{ Layout.fillHeight: true}
            Button{ text: "Ok"; onClicked: custom_color_window.visible = false }
        }
    }

    Rectangle {

        id : ai_is_thinking
        visible: gamemodel.currentPlayerType === 1 ? true : false

        anchors.fill: parent
        anchors.margins: 50


        color: "grey"
        opacity: 0.8

        ColumnLayout {
            anchors.fill: parent
            anchors.margins: 20
            Text{ Layout.preferredWidth: parent.width; height: 20;
                text: "AI is thinking, please wait!"; color: "red";
                font.family: "Garamond"; font.pointSize: 32; horizontalAlignment: "AlignHCenter"}
            Item{ height: 20}
            Item{ Layout.fillHeight: true}
            AnimatedImage {
                id: animation; source: "think.gif"; horizontalAlignment: "AlignHCenter"
            }
        }
    }
}

